CREATE EXTENSION IF NOT EXISTS earthdistance;
CREATE EXTENSION IF NOT EXISTS cube;

CREATE TABLE IF NOT EXISTS postal_codes(
    id serial PRIMARY key,
    country_code VARCHAR(2),
    postal_code VARCHAR(64),
    place_name_local VARCHAR(256),
    state_name_local VARCHAR(256),
    lat_long_location point
);

CREATE TABLE IF NOT EXISTS api_keys(
    id serial PRIMARY key,
    email VARCHAR(128),
    api_key VARCHAR(160)
);


import pandas as pd
import psycopg2 as pg
import argparse

def setup_db(db_name, db_user, db_password, db_host, db_port):

    df = pd.read_csv("./data/geonames-postal-code.csv", delimiter=";")

    connection = "dbname={db_name} user={db_user} password={db_password} host={db_host} port={db_port}"

    conn = pg.connect(format(connection, db_name=db_name, db_user=db_user, db_password=db_password, db_host=db_host, db_port=db_port))
    cur = conn.cursor()
    batch_size = 20
    for i in range(0, len(df), batch_size):
        batch = df[i:i+batch_size]
        for index, row in batch.iterrows():
            country_code = row["country code"]
            postal_code = row["postal code"]
            place_name_local = row["place name"]
            state_name_local = row["admin name1"]
            longitude = row["longitude"]
            latitude = row["latitude"]
        

            cur.execute("""INSERT INTO postal_codes (
                            country_code,
                            postal_code,
                            place_name_local,
                            state_name_local,
                            lat_long_location
                        )
                        VALUES(
                            %s,
                            %s,
                            %s,
                            %s,
                            POINT(%s, %s)
                        )
                        """, (country_code, postal_code, place_name_local, state_name_local, latitude, longitude))
        print(f"save {index + batch_size}", end="\r")
        conn.commit()

    cur.close()
    conn.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser("locations API - DB setup")
    parser.add_argument("--host", "-h",  help="Host IP of DB", type=str, default="0.0.0.0" )
    parser.add_argument("--password", "-p",  help="Password of DB", type=str, default="password")
    parser.add_argument("--user", "-u",  help="User of DB", type=str, default="postgres")
    parser.add_argument("--name", "-n",  help="Name of DB", type=str, default="postgres")
    parser.add_argument("--name", "-n",  help="Name of DB", type=str, default="postgres")
    args = parser.parse_args()

    setup_db(db_host=args.host, db_password=args.password, db_user=args.user, db_name=args.name, db_port=args.port)


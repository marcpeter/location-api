#!/bin/bash

program_name="main"

# Check if the program is running
if pgrep -f "$program_name" > /dev/null; then
    # Get the PID of the program
    pid=$(pgrep -f "$program_name")

    # Stop the program using pkill
    sudo pkill -f "$program_name"
    echo "Program '$program_name' (PID: $pid) stopped."
else
    echo "Program '$program_name' is not currently running."
fi
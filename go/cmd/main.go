package main

import (
	"log"

	"github.com/joho/godotenv"
	"info.marc-peter/locations_api/internal/repository"
	"info.marc-peter/locations_api/internal/router"
)

func init() {

	// load .env file
	err := godotenv.Load("./.env")
	if err != nil {
		log.Fatalf("Can not load .env file %v", err)
	}

}

func main() {
	repository.OpenDbConnection()
	router.Init()

}

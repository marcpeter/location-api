package middleware

import (
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"info.marc-peter/locations_api/internal/services"
)

func CheckApiKey() func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		apiHeader := ctx.GetHeader("x-api-key")

		fmt.Printf("header %v", apiHeader)
		valid, err := services.ValidApiKey(apiHeader)
		if !valid || err != nil {
			log.Panic(err)
			println("ABORT")
			ctx.Abort()
		}
		ctx.Next()

	}

}

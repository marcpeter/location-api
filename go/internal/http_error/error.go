package http_error

import "fmt"

type HttpError struct {
	Message    string `json:"message"`
	StatusCode int    `json:"status"`
	Metadata   string `json:"metadata"`
}

func NewHttpError(statusCode int, message string, metadata string) HttpError {
	return HttpError{
		Message:    message,
		StatusCode: statusCode,
		Metadata:   metadata,
	}
}

func (e HttpError) Error() string {
	return fmt.Sprintf("message: %s,  metadata: %s", e.Message, e.Metadata)
}

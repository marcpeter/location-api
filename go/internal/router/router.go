package router

import (
	"fmt"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"info.marc-peter/locations_api/internal/middleware"
	"info.marc-peter/locations_api/internal/services"
)

func Init() {
	ginRouter := gin.New()

	ginRouter.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "OPTIONS"},
		AllowHeaders:     append([]string{"x-api-key"}),
		AllowCredentials: true,
	}),
		middleware.ErrorHandler(),
	)

	ginRouter.GET("/valid", middleware.CheckApiKey(), services.GetValidation)
	ginRouter.GET("/location", middleware.CheckApiKey(), services.GetLocation)
	ginRouter.GET("/distance", middleware.CheckApiKey(), services.GetDistanceBetweenLocations)
	ginRouter.Run(fmt.Sprintf(":%s", os.Getenv("GIN_PORT")))

}

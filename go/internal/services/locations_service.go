package services

import (
	"errors"
	"math"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"info.marc-peter/locations_api/internal/http_error"
	"info.marc-peter/locations_api/internal/models"
	"info.marc-peter/locations_api/internal/repository"
)

func GetLocation(ctx *gin.Context) {
	postalCode := ctx.DefaultQuery("postalCode", "")
	countryCode := ctx.DefaultQuery("countryCode", "")
	placeName := ctx.DefaultQuery("place", "")
	stateName := ctx.DefaultQuery("state", "")
	if len(postalCode) < 2 && len(countryCode) == 0 && len(placeName) < 3 && len(stateName) < 3 {
		httpErr := http_error.NewHttpError(http.StatusBadRequest, "postalCode, countryCode, placeName and stateName ar invalid", "")
		ctx.AbortWithStatusJSON(http.StatusBadRequest, httpErr)
		return
	}
	var specs []*repository.Specification[any]
	if countryCode != "" {
		specs = append(specs, repository.SpecEquals("country_code", countryCode))
	}
	if postalCode != "" && len(postalCode) >= 3 {
		specs = append(specs, repository.SpecEquals("postal_code", postalCode))
	}
	if len(placeName) >= 4 {
		specs = append(specs, repository.SpecPrefixIgnoreCase("place_name_local", placeName))
	}
	if len(stateName) >= 4 {
		specs = append(specs, repository.SpecPrefixIgnoreCase("state_name_local", stateName))
	}
	spec := repository.SpecAnd(specs...)

	locs, err := repository.GetLocations(spec)
	if err != nil {
		ctx.Abort()
	}
	ctx.IndentedJSON(http.StatusOK, locs)
}

func GetLocationByCountryAndPostalCode(ctx *gin.Context) {
	postalCode := ctx.DefaultQuery("postalCode", "")
	countryCode := ctx.DefaultQuery("countryCode", "")
	if len(countryCode) != 2 {
		httpErr := http_error.NewHttpError(http.StatusBadRequest, "Invalid parameter countryCode", "")
		ctx.AbortWithStatusJSON(http.StatusBadRequest, httpErr)
		return
	}
	if len(postalCode) < 4 {
		httpErr := http_error.NewHttpError(http.StatusBadRequest, "Invalid parameter postalCode", "")
		ctx.AbortWithStatusJSON(http.StatusBadRequest, httpErr)
		return
	}
	spec := repository.SpecAnd(repository.SpecEquals("country_code", countryCode), repository.SpecEquals("postal_code", postalCode))
	loc, err := repository.GetLocation(spec)
	if err != nil {
		ctx.AbortWithStatusJSON(err.StatusCode, err)
		return
	}
	ctx.IndentedJSON(http.StatusOK, loc)
}

func extractLocation(locationTuple string) (countryCode string, postalCode string, err error) {
	tuple := strings.Split(locationTuple, ",")
	if len(tuple) != 2 {
		return "", "", errors.New("Invalid parameter locations")
	}
	return tuple[0], tuple[1], nil
}

func GetDistanceBetweenLocations(ctx *gin.Context) {
	location1 := ctx.DefaultQuery("location1", "")
	location2 := ctx.DefaultQuery("location2", "")
	system := ctx.DefaultQuery("sys", "metric")
	if location1 == "" {
		httpErr := http_error.NewHttpError(http.StatusBadRequest, "location1 is not set", "")
		ctx.AbortWithStatusJSON(http.StatusBadRequest, httpErr)
		return
	}
	if location2 == "" {
		httpErr := http_error.NewHttpError(http.StatusBadRequest, "location2 is not set", "")
		ctx.AbortWithStatusJSON(http.StatusBadRequest, httpErr)
		return
	}
	countryCode1, postalCode1, err := extractLocation(location1)
	if err != nil {
		httpErr := http_error.NewHttpError(http.StatusBadRequest, err.Error(), "")
		ctx.AbortWithStatusJSON(http.StatusBadRequest, httpErr)
		return
	}

	countryCode2, postalCode2, err := extractLocation(location2)
	if err != nil {
		httpErr := http_error.NewHttpError(http.StatusBadRequest, err.Error(), "")
		ctx.AbortWithStatusJSON(http.StatusBadRequest, httpErr)
		return
	}
	distance, httpErr := repository.GetDistance(countryCode1, postalCode1, countryCode2, postalCode2)
	if httpErr != nil {
		ctx.AbortWithStatusJSON(httpErr.StatusCode, httpErr)
		return
	}
	distance = distance / 1000
	unit := "km"
	if system == "imperial" {
		distance = distance * 0.621371
		unit = "miles"
	}
	distance = math.Floor(distance*100) / 100
	distanceObj := models.Distance{
		Distance: distance,
		Unit:     unit,
	}
	ctx.IndentedJSON(http.StatusOK, distanceObj)

}

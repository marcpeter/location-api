package services

import "github.com/gin-gonic/gin"

func GetQueryParam(ctx *gin.Context, key string, defaultVal *string) string {
	if defaultVal == nil {
		defaultVal = new(string)
		*defaultVal = "none"
	}
	return ctx.DefaultQuery(key, *defaultVal)
}

package services

import (
	"github.com/gin-gonic/gin"
	"info.marc-peter/locations_api/internal/repository"
)

func ValidApiKey(apiKey string) (bool, error) {
	count, err := repository.ValidateApiKey(apiKey)
	if err != nil {
		return false, err
	}
	if count == 1 {
		return true, nil
	}

	return false, nil
}

func GetValidation(ctx *gin.Context) {

}

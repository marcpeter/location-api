package repository

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

const placeHolder = "#?#"

type ValueType interface {
	string | int | float64
}

type Specification[T any] struct {
	query  string
	values []T
}

func SpecContains(colName string, value string) *Specification[any] {
	spec := &Specification[any]{
		query:  fmt.Sprintf("%s LIKE %s", colName, placeHolder),
		values: []any{"%" + value + "%"},
	}
	return spec
}
func SpecPrefix(colName string, value string) *Specification[any] {
	spec := &Specification[any]{
		query:  fmt.Sprintf("%s LIKE %s", colName, placeHolder),
		values: []any{value},
	}
	return spec
}
func SpecPrefixIgnoreCase(colName string, value string) *Specification[any] {
	spec := &Specification[any]{
		query:  fmt.Sprintf("LOWER(%s) LIKE %s", colName, placeHolder),
		values: []any{strings.ToLower(value + "%")},
	}
	return spec
}
func SpecContainsIgnoreCase(colName string, value string) *Specification[any] {
	spec := &Specification[any]{
		query:  fmt.Sprintf("LOWER(%s) LIKE %s", colName, placeHolder),
		values: []any{strings.ToLower("%" + value + "%")},
	}
	return spec
}

func SpecEquals(colName string, value any) *Specification[any] {
	spec := &Specification[any]{
		query:  fmt.Sprintf("%s=%s", colName, placeHolder),
		values: []any{value},
	}
	return spec
}

func SpecOr(specs ...*Specification[any]) *Specification[any] {
	orSpec := &Specification[any]{}
	if len(specs) == 1 {
		return specs[0]
	}
	for index, spec := range specs {
		if index == 0 {
			orSpec.query = fmt.Sprintf("(%s", spec.query)
		} else {
			orSpec.query += fmt.Sprintf(" OR %s", spec.query)
		}
		if index == len(specs)-1 {
			orSpec.query += ")"
		}
		orSpec.values = append(spec.values, spec.values...)
	}
	return orSpec

}
func SpecAnd(specs ...*Specification[any]) *Specification[any] {
	if len(specs) == 0 {
		return nil
	}

	andSpec := &Specification[any]{}
	if len(specs) == 1 {
		return specs[0]
	}
	for index, spec := range specs {
		println(spec.values)
		if index == 0 {
			andSpec.query = fmt.Sprintf("(%s", spec.query)
		} else {
			andSpec.query += fmt.Sprintf(" AND %s", spec.query)
		}
		if index == len(specs)-1 {
			andSpec.query += ")"
		}
		andSpec.values = append(andSpec.values, spec.values...)
	}
	return andSpec
}

func SpecBuild(spec *Specification[any]) (query string, values []any) {
	re := regexp.MustCompile(`#\?#`)
	counter := 1
	result := re.ReplaceAllStringFunc(spec.query, func(match string) string {
		replacement := "$" + strconv.Itoa(counter)
		counter++
		return replacement
	})
	return result, spec.values

}

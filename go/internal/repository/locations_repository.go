package repository

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"math"
	"net/http"

	"info.marc-peter/locations_api/internal/http_error"
	"info.marc-peter/locations_api/internal/models"
)

func GetLocations(spec *Specification[any]) ([]models.Location, error) {

	if spec == nil {
		return nil, errors.New("No query param specified")
	}
	query, values := SpecBuild(spec)

	rows, err := DB.Query(fmt.Sprintf("SELECT country_code, postal_code, place_name_local, state_name_local, lat_long_location FROM postal_codes WHERE %s", query), values...)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var locs []models.Location = []models.Location{}
	for rows.Next() {
		var loc models.Location
		var pointStr string
		if err := rows.Scan(&loc.CountryCode, &loc.PostalCode, &loc.PlaceNameLocal, &loc.StateNameLocal, &pointStr); err != nil {
			log.Panic(err)
		}
		point, err := models.ParseGeoLocation(pointStr)
		if err != nil {
			fmt.Printf("Error while parsing point %v\n", err)
			return nil, err
		}
		loc.GeoLocation = point
		locs = append(locs, loc)
	}
	return locs, nil
}

func GetLocation(spec *Specification[any]) (models.Location, *http_error.HttpError) {
	clause, values := SpecBuild(spec)
	var loc models.Location
	var pointStr string
	err := DB.QueryRow(fmt.Sprintf("SELECT country_code, postal_code, place_name_local, state_name_local, lat_long_location FROM postal_code WHERE %s LIMIT 1", clause), values...).
		Scan(&loc.CountryCode, &loc.PostalCode, &loc.PlaceNameLocal, &loc.StateNameLocal, &pointStr)
	if err != nil {
		if err == sql.ErrNoRows {
			httpError := http_error.NewHttpError(http.StatusNotFound, "No matching location found.", "")
			return models.Location{}, &httpError
		} else {
			httpError := http_error.NewHttpError(http.StatusInternalServerError, "Select location failed.", "")
			return models.Location{}, &httpError
		}
	}
	point, err := models.ParseGeoLocation(pointStr)
	if err != nil {
		httpError := http_error.NewHttpError(http.StatusInternalServerError, "Failed parsing geo location", "")
		return models.Location{}, &httpError
	}
	loc.GeoLocation = point
	return loc, nil
}

func GetDistance(countryCode1 string, postalCode1 string, countryCode2 string, postalCode2 string) (float64, *http_error.HttpError) {

	query := fmt.Sprintf(`
	SELECT
		earth_distance(
			ll_to_earth(l1.lat_long_location[0], l1.lat_long_location[1]),
			ll_to_earth(l2.lat_long_location[0], l2.lat_long_location[1])
		) AS distance_in_meters
	FROM
		postal_codes AS l1
	JOIN
		postal_codes AS l2
	ON
		l1.country_code=$1 AND
		l1.postal_code=$2 AND
		l2.country_code=$3 AND
		l2.postal_code=$4;
	`)

	var distanceInMeters float64
	err := DB.QueryRow(query, countryCode1, postalCode1, countryCode2, postalCode2).Scan(&distanceInMeters)
	if err != nil {
		httpError := http_error.NewHttpError(http.StatusInternalServerError, "Failed calculating distance", err.Error())
		return 0, &httpError
	}
	distanceInMeters = math.Round(distanceInMeters)
	return distanceInMeters, nil

}

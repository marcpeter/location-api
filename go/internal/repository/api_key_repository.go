package repository

func ValidateApiKey(apiKey string) (int, error) {
	var count int
	err := DB.QueryRow("SELECT COUNT(*) FROM api_keys WHERE api_key=$1", apiKey).Scan(&count)
	return count, err
}

package models

import (
	"fmt"
	"strconv"
	"strings"
)

type GeoLocation struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

func ParseGeoLocation(pointStr string) (GeoLocation, error) {
	var p GeoLocation
	// Remove parentheses and split the string into X and Y components
	parts := strings.Split(strings.Trim(pointStr, "()"), ",")
	if len(parts) != 2 {
		return p, fmt.Errorf("Invalid point format: %s", pointStr)
	}

	x, err := strconv.ParseFloat(parts[0], 64)
	if err != nil {
		return p, err
	}

	y, err := strconv.ParseFloat(parts[1], 64)
	if err != nil {
		return p, err
	}

	p.Latitude = x
	p.Longitude = y

	return p, nil
}

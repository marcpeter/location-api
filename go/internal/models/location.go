package models

type Location struct {
	CountryCode    string      `json:"countryCode"`
	PostalCode     string      `json:"postalCode"`
	PlaceNameLocal string      `json:"placeNameLocal"`
	StateNameLocal string      `json:"stateNameLocal"`
	GeoLocation    GeoLocation `json:"geoLocation"`
}

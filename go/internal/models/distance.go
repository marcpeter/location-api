package models

type Distance struct {
	Distance float64 `json:"distance"`
	Unit     string  `json:"unit"`
}
